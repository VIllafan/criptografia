const express= require('express')
const app = express()
const bcrypt = require('bcrypt');
const crypto = require("crypto-js")
const saltRounds = 10;
const KEY = '1234567812345678'
const myPlaintextPassword = 'Hola123$';
const someOtherPlaintextPassword = 'not_bacon';
app.listen(3000 , ()=>{
    console.log('SERVER up')
    bcrypt.genSalt(saltRounds,function(err,salt){
        bcrypt.hash(myPlaintextPassword,saltRounds , function(err,hash){
            console.log(hash)
            let encrypted = crypto.AES.encrypt(myPlaintextPassword,KEY)

            console.log(encrypted.ciphertext.toString());
        })
    })
});


