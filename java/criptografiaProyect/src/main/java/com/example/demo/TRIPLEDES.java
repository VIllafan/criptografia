package com.example.demo;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class TRIPLEDES {
private static final String KEY="5oquil2oo2vb63e8ionujny6";


public static String encrypt(String plainText) {
	try {
		SecretKeySpec keySpec = new SecretKeySpec(KEY.getBytes(), "DESede");
		Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		byte[] encrypted = cipher.doFinal(plainText.getBytes());
		return new String(Base64.getEncoder().encode(encrypted));
	} catch (Exception e) {
		return "Fallo: "+e.getMessage();
	}
	
	
}
public static String decrypt(String encrypText) {
	try {
		SecretKeySpec keySpec = new SecretKeySpec(KEY.getBytes(), "DESede");
		Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		byte[] decrypted = cipher.doFinal(encrypText.getBytes());
		return new String(decrypted);
	} catch (Exception e) {
		
		return "Fallo: "+e.getMessage();
	}
}
}
