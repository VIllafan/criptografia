package com.example.demo;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class AES {
private static final String KEY = "1234567812345678";



public static String encrypt(String textPlain) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
	SecretKeySpec keySpec = new SecretKeySpec(KEY.getBytes(), "AES");
	Cipher cipher;
	try {
		cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		return new String(Base64.getEncoder().encode(cipher.doFinal(textPlain.getBytes())));
	} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return e.getMessage();
	}
	
}


}
