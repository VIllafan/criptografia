package com.example.demo;

import java.security.InvalidKeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class CriptografiaProyectApplication {

	public static void main(String[] args) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		SpringApplication.run(CriptografiaProyectApplication.class, args);
		String toPass = "Hola123$";
		String decodePassword;
		String passfromNode = "$2b$10$pkYGU3YIOsNNIyeRaeIEee0l8UYHOWhwDP0S8Qka1u4.D1X9FIdgq";
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodedPassword = encoder.encode(toPass);
		System.out.println();
		System.out.println("Password is         : " + toPass);
		System.out.println("Encoded Password is : " + encodedPassword);
		System.out.println();
		boolean ispasswordMatch = encoder.matches(toPass, passfromNode);
		if(ispasswordMatch) {
			System.out.println("Se hizo match");
		}else {
			System.out.println("No Hizo match");
		}
		System.out.println("probando la encriptacion DESdes con el texto:  "+toPass);
		encodedPassword = TRIPLEDES.encrypt(toPass);
		System.out.println("Texto encriptado: "+encodedPassword);
		decodePassword = TRIPLEDES.decrypt(encodedPassword);
		System.out.println("Texto desencriptado: "+decodePassword);
		
		System.out.println("probando la encriptacion AES con el texto:  "+toPass);
		encodedPassword = AES.encrypt(toPass);
		System.out.println("Texto encriptado: "+encodedPassword);
	}
	
	
	
	

}
